ICML Test-of-Time Award Paper (a.k.a. 10 Year Paper):
 
 - [A Unified Architecture for Natural Language Processing: Deep Neural Networks with Multitask Learning](icml/icml08.pdf). Ronan Collobert, Jason Weston. [ICML'18](https://icml.cc/2018)

 - [Combining Online and Offline Knowledge in UCT](icml/icml07.pdf). Sylvain Gelly, David Silver, [ICML'17](https://icml.cc/Conferences/2017/Awards)

 - [Dynamic Topic Models](icml/icml06.pdf). David M. Blei, John D. Lafferty. [ICML'16](https://icml.cc/Conferences/2016/index.html%3Fp=2009.html)

 - [Learning to Rank Using Gradient Descent](icml/icml05.pdf). Chris Burges, Tal Shaked, Erin Renshaw, Ari Lazier, Matt Deeds, Nicole Hamilton, Greg Hullender. ICML'05. [ICML'15](https://icml.cc/2015/index.html%3Fp=51.html)

 - [Multiple kernel learning, conic duality, and the SMO algorithm](icml/icml04.pdf). Francis Bach; Gert Lanckriet; Michael Jordan. [ICML'14](https://icml.cc/2014/index/article/26.htm)

 - [Semi-Supervised Learning Using Gaussian Fields and Harmonic Functions](icml/icml03a.pdf). Xiaojin Zhu,  Zoubin Ghahramani, and John Lafferty. 
  [Online Convex Programming and Generalized Infinitesimal Gradient Ascent](icml/icml03b.pdf). Martin Zinkevich. [ICML'13](https://icml.cc/2013/index.html%3Fpage_id=21.html)

 - [Diffusion Kernels on Graphs and Other Discrete Input Spaces](icml/icml02.pdf). Risi Kondor, John Lafferty. [ICML'12](https://icml.cc/2012/schedule.1.html)

 - [Conditional Random Fields: Probabilistic Models for Segmenting and Labeling Sequence Data](icml/icml01.pdf). John D. Lafferty, Andrew McCallum, Fernando C. N. Pereira. [ICML'11](https://icml.cc/2011/schedule.php.html)

 - [Reducing Multiclass to Binary: A Unifying Approach for Margin Classifiers](icml/icml00.pdf). Erin L. Allwein, Robert E. Schapire, Yoram Singer. [ICML'10](https://icml.cc/2010/awards.html)

 - [Transductive Inference for Text Classification using Support Vector Machines](icml/icml99.pdf). Thorsten Joachims. [ICML'09](https://icml.cc/2009/awards.html)
